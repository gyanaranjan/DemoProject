package com.example.gyana.alltestingapp.Fragment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.gyana.alltestingapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class SearchviewFirst extends AppCompatActivity {
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchview_first);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchviewFirst.this,MainActivitySecond.class);
                startActivity(intent);
            }
        });
        String data = "{'shippng_address':{'phone':'67719657','name':'Lucy Middleton','Address':'salmiya Block 10, Street No3, Floor No 6, Flat No 22, Kuwait'},'billing_address':{'phone':'67719657','name':'Lucy Middleton','Address':'salmiya Block 10, Street No3, Floor No 6, Flat No 22, Kuwait'},'StoreData':[{'storeId':115,'storeName':'LOFT415','logo':'http://dev.elitabi.com/uploads/product/555x555/PDIMG691851511866251.jpg','shippingDay':'Ready to ship in 3-5 business days','shippingFrom':'from United State','sellerTtlPrice':'KD 32.000', 'products': [{'proId':100,'proName':'Black Solid Sweat Jacket','proImg':'http://dev.elitabi.com/uploads/product/555x555/PDIMG691851511866251.jpg', 'proSize':'Large','proColor':'Gray','processDay':'3 Days','proQty':1, 'optNote':'Seller Note Will Go Here','proPrice':'KD 12.000','proEachPrice':'(KD 10.000 Each)','status':'Delivered','delivere_data':'on 23rd nov 2016' },{'proId':104,'proName':'Black Solid Sweat Jacket','proImg':'http://dev.elitabi.com/uploads/product/555x555/PDIMG288511509086993.jpg','proSize':'Medium','proColor':'Red','processDay':'3 Days','proQty':2,'optNote':'Seller Note Will Go Here','proPrice':'KD 20.000','proEachPrice':'','status':'Pending','delivere_data':''}]},{'storeId':116,'storeName':'WEARHG','logo':'http://dev.elitabi.com/uploads/vendors/orig/VD498821509183479_logo658301511607607.jpg','shippingDay':'Ready to ship in 2-3 business days','shippingFrom':'from United State','sellerTtlPrice':'KD 19.000','products': [{'proId':110,'proName':'Purple Solid Hoodie','proImg':'http://dev.elitabi.com/uploads/product/555x555/PDIMG153681506777964.jpg','proSize':'Medium','proColor':'Black','processDay':'2 Days','proQty':1,'optNote':'Optional Note to Seller','proPrice':'KD 9.000','proEachPrice':'','status':'Delivered','delivere_data':'on 23rd nov 2016'}]}],'Status':true}";

        try {
            JSONObject jsonO = new JSONObject(data);
            Iterator iterator = jsonO.keys();

            while (iterator.hasNext()){
                String key = iterator.next().toString();
                String value = jsonO.get(key).toString();
                if (key.equals("StoreData")){
                    JSONArray jsonArray = new JSONArray(value);
                    for (int k=0;k<jsonArray.length();k++){
                        JSONObject kl = jsonArray.getJSONObject(k);
                        Log.v("OBJECT",""+kl);
                    }
                }
            }



        }catch (Exception e){
            e.printStackTrace();
        }

        HashMap hashMap = new HashMap();
        hashMap.put("first_name","Gyana");
        hashMap.put("last_name","mohapatra");
        hashMap.put("email","abcd@gmail.com");
        hashMap.put("language","english");
        hashMap.put("country","kuwait");
        hashMap.put("profile_image","image_url");

        JSONObject jsonObject = new JSONObject(hashMap);
        Log.v("OBJECT",""+jsonObject);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.first,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.m1){
            Intent i = new Intent(this,MainActivitySecond.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
