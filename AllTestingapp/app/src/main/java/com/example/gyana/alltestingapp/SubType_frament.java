package com.example.gyana.alltestingapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class SubType_frament extends Fragment {


    View view;
    Button firstButton;
    ListView listView;
    ArrayList<String> arrayList;
    String userId="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();




    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //adding firstfragment layout whenneeded
        // view = inflater.inflate(R.layout.activity_first_fragment, container, false);
        view = inflater.inflate(R.layout.fragment_sub_type_frament, container, false);

        listView = (ListView)view.findViewById(R.id.listview);
        // CustomlistAdapter customlistAdapter = new CustomlistAdapter(getActivity(),);
       /* firstButton = (Button) view.findViewById(R.id.firstButton);
        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        Bundle args = getArguments();

        if (args  != null && args.containsKey("id_User")){
            userId = args.getString("id_User");
            Toast.makeText(getActivity(), userId, Toast.LENGTH_SHORT).show();
        }
        Log.v("SIPUN","First_U");
        arrayList = new ArrayList<>();
        if (userId.equals("Cricket player")){
            arrayList.add("Sachin");
            arrayList.add("Sehawge");
            arrayList.add("Dhoni");
            arrayList.add("maxwell");
        } else if (userId.equals("Country")){
            arrayList.add("India");
            arrayList.add("Pak");
            arrayList.add("Aus");
        } else if (userId.equals("Football player")){
            arrayList.add("Messi");
            arrayList.add("Ronaldo");
            arrayList.add("Kaka");
            arrayList.add("Neymar");

        } else if (userId.equals("chess")){
            arrayList.add("Annand");
            arrayList.add("padmin");

        }
        Customtest cst = new Customtest(getActivity(),arrayList);
        listView.setAdapter(cst);

        return view;
    }
}
class Customtest extends BaseAdapter{
    ArrayList<String> arrayList;
    Context context;
    public Customtest(Context context,ArrayList<String> arrayList){
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.itemtype, parent, false);

        TextView text = (TextView)convertView.findViewById(R.id.textview);
        text.setText(arrayList.get(position));
        text.setTextSize(40);
        return convertView;
    }
}