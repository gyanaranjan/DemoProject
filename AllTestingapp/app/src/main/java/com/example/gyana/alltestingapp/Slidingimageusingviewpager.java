package com.example.gyana.alltestingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.*;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Slidingimageusingviewpager extends AppCompatActivity {
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slidingimageusingviewpager);
        viewPager = (ViewPager)findViewById(R.id.viewpage);
        Integer[] imageId = {R.drawable.google2, R.drawable.google1, R.drawable.google3, R.drawable.ic_home_black_24dp};
        String[] imagesName = {"image1","image2","image3","image4","image5"};


        viewPager = (ViewPager) findViewById(R.id.viewpage);
        viewPagerAdapter adapter = new viewPagerAdapter(this,imageId,imagesName);
        viewPager.setAdapter(adapter);
        final Handler h = new Handler(Looper.getMainLooper());
        final Runnable r = new Runnable() {
            public void run() {
                viewPager.setCurrentItem(0, true);
            }
        };
        h.postDelayed(r, 500);
    }
}

class viewPagerAdapter extends PagerAdapter{

    private Context context;
    private Integer[] imagesArray;
    private String[] namesArray;

    public viewPagerAdapter(Context context, Integer[] imagesArray, String[] namesArray){

        this.context = context;
        this.imagesArray = imagesArray;
        this.namesArray = namesArray;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.imageitem, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.imageView);
        imageView.setImageResource(imagesArray[position]);
        TextView textView1 = (TextView) viewItem.findViewById(R.id.textview);
        textView1.setText(namesArray[position]);
        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return imagesArray.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((View)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}