package com.example.gyana.alltestingapp.Fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.gyana.alltestingapp.R;

import java.util.ArrayList;

public class OutputFragment extends Fragment {
    SearchView output;
    ListView listView;
    ArrayList arrayList = new ArrayList();
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_output, viewGroup, false);
        output= (SearchView)view.findViewById(R.id.word);
        listView = (ListView)view.findViewById(R.id.listview);
        arrayList.add("Mens");
        arrayList.add("Womens");
        arrayList.add("kids");
        OutputFragmentAdapter outputFragmentAdapter = new OutputFragmentAdapter(getActivity(),arrayList);
        listView.setAdapter(outputFragmentAdapter);
       /* ArrayAdapter adapter = new ArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_activated_1, arrayList);*/
        listView.setAdapter(outputFragmentAdapter);
        return view;
    }
    public void display(String txt){
        if (txt.equals("0")){
            output.setVisibility(View.VISIBLE);
            arrayList.clear();
            arrayList.add("Mens");
            arrayList.add("Womens");
            arrayList.add("kids");
        }else if (txt.equals("1")){
            output.setVisibility(View.GONE);
            arrayList.clear();
            arrayList.add("Any Where");
            arrayList.add("Kuwait");
            arrayList.add("Custom");
        }
        else if (txt.equals("2")){
            output.setVisibility(View.GONE);
            arrayList.clear();
            arrayList.add("Item Type1");
            arrayList.add("Item Type2");
            arrayList.add("Item Type3");

        }
        else if (txt.equals("3")){
            output.setVisibility(View.GONE);
            arrayList.clear();
            arrayList.add("0-100");
            arrayList.add("100-200");
            arrayList.add("200-300");
            arrayList.add("300-400");
            arrayList.add("400-500");
            arrayList.add("500-600");
            arrayList.add("600-700");
            arrayList.add("700-1000");

        }else if (txt.equals("4")){
            output.setVisibility(View.VISIBLE);
            arrayList.clear();
            arrayList.add("Wrangler");
            arrayList.add("Levis");
            arrayList.add("Denim");
            arrayList.add("Leecoper");
            arrayList.add("Nike");
            arrayList.add("Woodland");
            arrayList.add("mustache");
            arrayList.add("PeterEngland");

        }else if (txt.equals("5")){
            output.setVisibility(View.GONE);
            arrayList.clear();
            arrayList.add("Red");
            arrayList.add("White");
            arrayList.add("Yellow");
            arrayList.add("pink");
            arrayList.add("blue");
            arrayList.add("green");
        }
        else if (txt.equals("6")){
            output.setVisibility(View.GONE);
            arrayList.clear();
            arrayList.add("Ordering Options 1");
            arrayList.add("Ordering Options 2");
            arrayList.add("Ordering Options 3");

        }
        else if (txt.equals("7")){
            arrayList.clear();
            arrayList.add("Ship to 1");
            arrayList.add("Ship to 2");
            arrayList.add("Ship to 3");
            arrayList.add("Ship to 4");
            arrayList.add("Ship to 5");

        }
        OutputFragmentAdapter outputFragmentAdapter = new OutputFragmentAdapter(getActivity(),arrayList);
        listView.setAdapter(outputFragmentAdapter);
    }
}

class OutputFragmentAdapter extends BaseAdapter{
    Context context;
    ArrayList arrayList;
public OutputFragmentAdapter(Context context,ArrayList arrayList){
    this.arrayList = arrayList;
    this.context = context;
}
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.itemtype, parent, false);
        }
        TextView textView = (TextView)convertView.findViewById(R.id.textview);
        EditText editText = (EditText)convertView.findViewById(R.id.edittext);
        textView.setText((String)arrayList.get(position));
        if (arrayList.get(position).equals("Custom")){
            editText.setVisibility(View.VISIBLE);
        }else {
            editText.setVisibility(View.GONE);
        }
        return convertView;
    }
}