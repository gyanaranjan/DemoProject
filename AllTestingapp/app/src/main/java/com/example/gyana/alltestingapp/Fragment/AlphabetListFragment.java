package com.example.gyana.alltestingapp.Fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.gyana.alltestingapp.R;

import java.util.ArrayList;
import java.util.Arrays;

public class AlphabetListFragment extends ListFragment {
    String[] alphabet = new String[] { "CATEGORY","SHOP LOCATION","ITEM TYPE","PRICE","BRANDS","COLORS","ORDERING OPTIONS","SHIPING TO"};
   ArrayList<String> arrayList = new ArrayList();

    //String[] word = new String[]{"Apple", "Boat", "Cat"};
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_alphabet_list, viewGroup, false);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_activated_1, alphabet);
        setListAdapter(adapter);
        return view;
    }
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        OutputFragment txt = (OutputFragment)getFragmentManager().findFragmentById(R.id.output);
        txt.display(""+position);
        getListView().setSelector(android.R.color.holo_red_dark);
    }
}