package com.example.gyana.alltestingapp.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gyana.alltestingapp.R;

import java.util.ArrayList;

public class LetterFragment extends Fragment {

    ListView listView;
    ArrayList<String> arrayList = new ArrayList<>();
    View view;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_letter, container, false);
        listView = (ListView)view.findViewById(R.id.letterfrag);
        arrayList.add("A");
        arrayList.add("B");
        arrayList.add("C");
        arrayList.add("D");

        LetterFragmentadapter letterFragmentadapter = new LetterFragmentadapter(getActivity(),arrayList);
        listView.setAdapter(letterFragmentadapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WordFragment wordFragment = (WordFragment)getFragmentManager().findFragmentById(R.id.output);
                wordFragment.display(String.valueOf(position));
                listView.setSelector(android.R.color.holo_red_dark);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}

class LetterFragmentadapter extends BaseAdapter {
    Context context;
    ArrayList arrayList;
    public LetterFragmentadapter(Context context,ArrayList arrayList){
        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.itemtype, parent, false);
        }
        TextView textView = (TextView)convertView.findViewById(R.id.textview);
        EditText editText = (EditText)convertView.findViewById(R.id.edittext);
        textView.setText((String)arrayList.get(position));

        return convertView;
    }
}