package com.example.jiban.registrationpageexamplebypatisir;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    EditText firstname, lastname, eml, phone1, phone2, district1, village1, post1, post2, district2, village2, age, facebookid, twitterid;
    EditText aadharno,password,edt_conformpassword;
    Button submit, selectimage;
    RadioGroup radioGroup;
    RadioButton gender,male,female,others;

    CheckBox socialMedia, graphicsDesign, citizenSurvey, telleCalling;
    ImageView imageView1;
    int selectid;
    String val;
    CheckBox address;
    String village,post,district;
    int request_code = 10;
    int same_as=0;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    int sum_category;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private String userChoosenTask;
    boolean isValidation = false;
    ProgressDialog progressDialog;
    CommonUtilities commonUtilities;
    Bitmap thumbnail;
    boolean isChecked;
    ArrayList check_box_list = new ArrayList();
    ImageView facebook_icon,twitter_icon;
    Spinner spinner;
    ArrayList user_type = new ArrayList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        commonUtilities = new CommonUtilities();
        firstname = (EditText) findViewById(R.id.edt_FirstName);
        lastname = (EditText) findViewById(R.id.edt_LastName);
        phone1 = (EditText) findViewById(R.id.edt_phone);
        phone2 = (EditText) findViewById(R.id.edt_phone2);
        eml = (EditText) findViewById(R.id.edt_email);
        password=(EditText)findViewById(R.id.edt_password);
        age = (EditText) findViewById(R.id.edt_age);
        village1 = (EditText) findViewById(R.id.edt_village);
        post1 = (EditText) findViewById(R.id.edt_post);
        district1 = (EditText) findViewById(R.id.edt_district);
        village2 = (EditText) findViewById(R.id.edt_village2);
        post2 = (EditText) findViewById(R.id.edt_post2);
        district2 = (EditText) findViewById(R.id.edt_district2);
        facebookid = (EditText) findViewById(R.id.editText4);
        twitterid = (EditText) findViewById(R.id.edt_twitterID);
        aadharno=(EditText) findViewById(R.id.edt_aadharno);
        submit = (Button) findViewById(R.id.submit);
        selectimage = (Button) findViewById(R.id.imageButton);
        imageView1 = (ImageView) findViewById(R.id.image);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
        selectid = radioGroup.getCheckedRadioButtonId();
        gender = (RadioButton) findViewById(selectid);
        socialMedia = (CheckBox) findViewById(R.id.socialmedia);
        graphicsDesign = (CheckBox) findViewById(R.id.graphicsdesign);
        citizenSurvey = (CheckBox) findViewById(R.id.citizensurvey);
        telleCalling = (CheckBox) findViewById(R.id.tellecalling);
        address=(CheckBox)findViewById(R.id.same_as_above);
        male=(RadioButton)findViewById(R.id.radioButton1);
        female=(RadioButton)findViewById(R.id.radioButton2);
        others=(RadioButton)findViewById(R.id.radioButton3);
        facebook_icon = (ImageView)findViewById(R.id.icon);
        twitter_icon = (ImageView)findViewById(R.id.icon1);
        edt_conformpassword = (EditText)findViewById(R.id.edt_conformpassword);
        spinner = (Spinner)findViewById(R.id.spinner);
        password.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edt_conformpassword.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        user_type = new ArrayList();
        user_type.add("Select Type");
        ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, user_type);
        spinner.setAdapter(adapter);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(address.isChecked()){
                    same_as = 1;
                    village=village1.getText().toString();
                    post=post1.getText().toString();
                    district=district1.getText().toString();
                    village2.setText(village);
                    post2.setText(post);
                    district2.setText(district);

                }else
                {
                    same_as=0;
                   village2.setText("");
                   post2.setText("");
                   district2.setText("");
                }
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        facebook_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert(MainActivity.this,"Login to facebook and provide the userid linked to your facebook profile");
            }
        });
        twitter_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert(MainActivity.this,"Login to twitter and provide the userid linked to your twitter profile");

            }
        });
        socialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    check_box_list.add("1");

                }else {
                    check_box_list.remove("1");

                }
            }
        });
        citizenSurvey.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    check_box_list.add("2");
                }else {
                    check_box_list.remove("2");

                }
            }
        });
        graphicsDesign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    check_box_list.add("4");
                }else {
                    check_box_list.remove("4");
                }
            }
        });

        telleCalling.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isChecked){
                    check_box_list.add("8");
                }else {
                    check_box_list.remove("8");
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)findViewById(checkedId);
                 isChecked =  checkedRadioButton.isChecked();

            }
        });


        selectimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = false;

                //Name field validation

                if (firstname.getText().toString().length() == 0) {
                    firstname.setError("Lastname field required");
                    isValidation = true;
                }
                if (lastname.getText().toString().length() == 0) {
                    lastname.setError("Lastname field required");
                    isValidation = true;
                }
                //Phone field validation
                int lenthh = phone1.length();
                if (lenthh == 0) {
                    isValidation = true;
                    phone1.setError("Phone number required");
                } else {
                    //isValidation = false;
                    if (lenthh != 10) {
                        isValidation = true;
                        phone1.setError("Phone must be 10 digit");
                    }
                }
                //Email validation
                String email = eml.getText().toString();
                int len = eml.length();
                int atLastindex = email.lastIndexOf("@");
                int dotLastindex = email.lastIndexOf(".");
                int atposition = email.indexOf("@");
                int dotposition = email.indexOf(".");

                if (len == 0) {
                    isValidation = true;
                    eml.setError("Please enter email");
                } else {
                   // isValidation = false;
                    if (atposition == 0 || dotposition == 0 || atposition == len - 1 || dotposition == len - 1 || atposition == -1 || dotposition == -1) {

                        eml.setError("Invalid emailid");
                        isValidation = true;
                    }

                }
                if(password.getText().toString().length()==0)
                {
                    password.setError("Password required");
                }else
                {
                   // isValidation=false;
                    if(password.getText().toString().length() <6)
                    {
                        isValidation=true;
                        password.setError("Password length atleast 6 charecter");
                    }
                }
                if (edt_conformpassword.getText().toString().length()==0){
                    edt_conformpassword.setError("confirm password required");
                    isValidation=true;
                }else {
                    if (!password.getText().toString().equals(edt_conformpassword.getText().toString())){
                        isValidation=true;
                        Toast.makeText(MainActivity.this, "Please match password and confirm password", Toast.LENGTH_SHORT).show();
                    }
                }
               if (!isChecked) {
                    isValidation = true;
                    Toast.makeText(MainActivity.this, "Please select gender", Toast.LENGTH_SHORT).show();

                }else {
                  // isValidation = false;
                   if (selectid==R.id.radioButton1) {
                       isValidation = true;
                       Toast.makeText(MainActivity.this,"You select Male", Toast.LENGTH_LONG).show();
                   }
                   if (selectid == R.id.radioButton2) {
                       isValidation = true;
                       Toast.makeText(MainActivity.this, "You select Female", Toast.LENGTH_LONG).show();
                   }
               }



                if (age.getText().toString().length() == 0) {
                    isValidation = true;
                    age.setError("Enter your age");
                }

                if (village1.getText().toString().length() == 0) {
                    isValidation = true;
                    village1.setError("Enter your village");
                }
                if (post1.getText().toString().length() == 0) {
                    isValidation = true;
                    post1.setError("Enter your post");
                }
                if (district1.getText().toString().length() == 0) {
                    isValidation = true;
                    district1.setError("Enter your district");
                }
                if (village2.getText().toString().length() == 0) {
                    isValidation = true;
                    village2.setError("Enter your village");
                }
                if (post2.getText().toString().length() == 0) {
                    isValidation = true;
                    post2.setError("Enter your post");
                }
                if (district2.getText().toString().length() == 0) {
                    isValidation = true;
                    district2.setError("Enter your district");
                }
                if(aadharno.getText().toString().length()==0){
                    isValidation=true;
                   aadharno.setError("Enter your Aadharno");
                }else
                {
                    //isValidation=false;
                    if(aadharno.getText().toString().length()!=12)
                    {
                        isValidation=true;
                        aadharno.setError("Aadhar no must be 12 digit");
                    }
                }
                /*checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                       Toast.makeText(getApplicationContext(),"123",Toast.LENGTH_LONG);
                    }
                });*/




                if (thumbnail == null) {
                    isValidation = true;
                    Toast.makeText(getApplicationContext(), "Please select Image", Toast.LENGTH_LONG).show();

                }
                if (!isValidation) {
                    try {
                        sendRequestToServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    int size = check_box_list.size();
                    Toast.makeText(getApplicationContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();

                }
            }

        });


    }
    private void sendRequestToServer() throws JSONException {
        progressDialog = ProgressDialog.show(MainActivity.this,"","please wait");

        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] array=byteArrayOutputStream.toByteArray();
        String image= Base64.encodeToString(array,Base64.DEFAULT);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("first_name",firstname.getText().toString());
        jsonObject.put("last_name",lastname.getText().toString());
        jsonObject.put("mobile",phone1.getText().toString());
        jsonObject.put("telephone",phone2.getText().toString());
        jsonObject.put("email_address",eml.getText().toString());
        jsonObject.put("password",password.getText().toString());
        jsonObject.put("repassword",edt_conformpassword.getText().toString());
        jsonObject.put("age",age.getText().toString());
        if(male.isChecked())
        {
            jsonObject.put("gender",1);
        }else if(female.isChecked())
        {
            jsonObject.put("gender",2);
        }
        else
        {
            jsonObject.put("gender",3);
        }
        for (int i=0;i<check_box_list.size();i++){
            String x = check_box_list.get(i).toString();
            sum_category = sum_category+Integer.parseInt(x);
        }
        jsonObject.put("volunter",sum_category);
        jsonObject.put("village",village1.getText().toString());
        jsonObject.put("ps",post1.getText().toString());
        jsonObject.put("dist",district1.getText().toString());
        jsonObject.put("p_village",village2.getText().toString());
        jsonObject.put("p_ps",post2.getText().toString());
        jsonObject.put("p_dist",district2.getText().toString());
        jsonObject.put("fb_id",facebookid.getText().toString());
        jsonObject.put("twitter",twitterid.getText().toString());
        jsonObject.put("adhar_no",aadharno.getText().toString());
        jsonObject.put("same_as",same_as);


       /* jsonObject.put("socialMedia",socialMedia.isChecked());
        jsonObject.put("graphicsDesign",graphicsDesign.isChecked());
        jsonObject.put("citizenSurvey",citizenSurvey.isChecked());
        jsonObject.put("telleCalling",telleCalling.isChecked());*/
        jsonObject.put("profile_image",image);



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,CommonUtilities.LOCAL,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
                            String status = response.getString("status");
                            if (status.contains("success")) {
                                Intent intent =new Intent(MainActivity.this,ResultActivity .class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        commonUtilities.showVolleyErrorMessage(MainActivity.this, error, getResources().getString(R.string.network_unavailable));
                    }
                }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                commonUtilities.showNewtworkResponse(response);
                return super.parseNetworkResponse(response);
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(50),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "TEST");

    }
   /* @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Open Camera"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }*/

    private void selectImage() {
        final CharSequence[] items = { "Open Camera", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Uoload Photo!");
        builder.setCancelable(false);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(MainActivity.this);

                if (items[item].equals("Open Camera")) {
                    userChoosenTask ="Open Camera";
//                    if(result)
//                        cameraIntent();
                    final String permission = android.Manifest.permission.CAMERA;
                    if (ContextCompat.checkSelfPermission(MainActivity.this, permission)
                            != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
//                showPermissionRationaleDialog("Test", permission);
                            requestForPermission(permission);
                        } else {
                            requestForPermission(permission);
                        }
                    } else {
                        launch();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask ="Choose from Gallery";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void launch() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, request_code);
    }

    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, REQUEST_CAMERA_PERMISSION);
    }
    private void galleryIntent()
    {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
        }
        Intent gallery = new Intent();
        gallery.setType("image/*");
        gallery.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(gallery, SELECT_FILE);
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);//
//        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == request_code)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imageView1.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageView1.setImageBitmap(thumbnail);
    }


    public  void showAlert(final Context context, String msg) {

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setMessage(msg);
        ;
        final String finalMsg = msg;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
//        builder.show();
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);

    }

}

