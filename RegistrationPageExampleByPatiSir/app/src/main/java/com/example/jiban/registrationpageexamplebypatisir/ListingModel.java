package com.example.jiban.registrationpageexamplebypatisir;

/**
 * Created by Jiban on 26-Feb-18.
 */

public class ListingModel {
    String firstname;
    String lastname;
    String email;
    int phoneno1;
    int phoneno2;
    int gender;
    int age;
    String village1;
    String post1;
    String district1;
    String village2;
    String post2;
    String district2;
    String facebook;
    String twitter;
    String imgurl;

    public ListingModel() {
    }


    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public int getPhone1() {
        return phoneno1;
    }
    public void setPhone1(int phoneno1) {
        this.phoneno1 = phoneno1;
    }
    public int getPhone2() {
        return phoneno2;
    }
    public void setPhone2(int phoneno2) {
        this.phoneno2 = phoneno2;
    }

    public int getGender(){
        return gender;
    }
    public void setGender(int gender)
    {
        this.gender=gender;
    }
    public int getAge()
    {
        return age;
    }
    public void setAge(int age)
    {
        this.age=age;
    }
    public  String getVillage1()
    {
        return village1;
    }
    public void setVillage1(String village1)
    {
        this.village1=village1;
    }
    public  String getPost1()
    {
        return post1;
    }
    public void setPost1(String post1)
    {
        this.post1=post1;
    }
    public  String getDistrict1()
    {
        return district1;
    }
    public void setDistrict1(String district1)
    {
        this.district1=district1;
    }
    public  String getVillage2()
    {
        return village2;
    }
    public void setVillage2(String village2)
    {
        this.village2=village2;
    }
    public  String getPost2()
    {
        return post2;
    }
    public void setPost2(String post2)
    {
        this.post2=post2;
    }
    public  String getDistrict2()
    {
        return district2;
    }
    public void setDistrict2(String district2)
    {
        this.district2=district2;
    }
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }




    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }





}



