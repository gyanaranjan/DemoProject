package com.example.gyana.alltestingapp;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class TablayoutusingViewpager extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablayoutusing_viewpager);
        viewPager = (ViewPager) findViewById(R.id.viewpage);
        tabs = (TabLayout) findViewById(R.id.tabs);

        TabLayout.Tab firstTab = tabs.newTab();
        firstTab.setText("First");
        tabs.addTab(firstTab);

        TabLayout.Tab secondTab = tabs.newTab();
        secondTab.setText("Second");
        tabs.addTab(secondTab);

        TabLayout.Tab thirdTab = tabs.newTab();
        thirdTab.setText("Third"); // set the Text for the first Tab
        tabs.addTab(thirdTab);
        PagerAdapter2 adapter = new PagerAdapter2(getSupportFragmentManager(),tabs.getTabCount());
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);

// addOnPageChangeListener event change the tab on slide


}
}
class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
class PagerAdapter2 extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter2(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FirstFragment tab1 = new FirstFragment();
                return tab1;
            case 1:
                SecondFragment tab2 = new SecondFragment();
                return tab2;
            case 2:
                ThirdFragment tab3 = new ThirdFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Frag1";
            case 1:
                return "frag2";
            case 2:
                return "Frag3";

            default:
                return null;
        }
    }
}




