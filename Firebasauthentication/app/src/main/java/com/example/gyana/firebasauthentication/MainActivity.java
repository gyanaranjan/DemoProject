package com.example.gyana.firebasauthentication;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
Button google_login,phone_login,facebook_login;
SQLiteDatabase sqLiteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataBaseHandler dataBaseHandler = new DataBaseHandler(MainActivity.this);
        dataBaseHandler.getWritableDatabase();
        sqLiteDatabase = MainActivity.this.openOrCreateDatabase(DataBaseHandler.DATABASE_NAME, MODE_PRIVATE, null);

        google_login = (Button)findViewById(R.id.google_login);
        phone_login = (Button)findViewById(R.id.phone_login);
        facebook_login = (Button)findViewById(R.id.facebook_login);
        google_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(MainActivity.this,googleauthentication.class);
                startActivity(intent);
            }
        });
        phone_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(MainActivity.this,PhoneAuthentication.class);
                startActivity(intent);
            }
        });
        facebook_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,FaceBookLogin.class);
                startActivity(intent);
            }
        });
    }
}
