package com.example.gyana.alltestingapp;

import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class Vewpagerwithtablayout extends AppCompatActivity {

    FrameLayout simpleFrameLayout;
    TabLayout tabLayout;
    public SQLiteDatabase DB;
    CommonUtilities commonUtilities;
    ViewPager simpleViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vewpagerwithtablayout);
        final DatabaseHandler db = new DatabaseHandler(Vewpagerwithtablayout.this);
        db.getWritableDatabase();
        DB = Vewpagerwithtablayout.this.openOrCreateDatabase(CommonUtilities.DATABASE_NAME, MODE_PRIVATE, null);
        commonUtilities = new CommonUtilities();

        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);
        simpleViewPager = (ViewPager)findViewById(R.id.simpleViewPager);

        CustomAdapter adapter = new CustomAdapter(getSupportFragmentManager());
        simpleViewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(simpleViewPager);
       // simpleViewPager.addOnPageChangeListener(new TabLayoutExample.TabLayoutOnPageChangeListener(tabLayout));

    }
}
class CustomAdapter extends FragmentStatePagerAdapter {
    int count;
    String[] names;
    public CustomAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FirstFragment tab1 = new FirstFragment();
                return tab1;
            case 1:
                SecondFragment tab2 = new SecondFragment();
                return tab2;
            case 2:
                ThirdFragment tab3 = new ThirdFragment();
                return tab3;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Frag1";
            case 1:
                return "frag2";
            case 2:
                return "Frag3";

            default:
                return null;
        }
    }
    }
