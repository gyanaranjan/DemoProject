package com.example.gyana.alltestingapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Test_main extends AppCompatActivity {
ViewPager viewpagers;
    TabLayout tableLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_main);
        viewpagers = (ViewPager)findViewById(R.id.viewpagers);
        tableLayout = (TabLayout)findViewById(R.id.tabLay);
        ViewPagerAdapter1  viewPagerAdapter = new ViewPagerAdapter1(getSupportFragmentManager());
        viewpagers.setAdapter(viewPagerAdapter);
        //tableLayout = (TabLayout) findViewById(R.id.tabs);
        tableLayout.setupWithViewPager(viewpagers);


    }

}
class ViewPagerAdapter1 extends FragmentStatePagerAdapter {

    public ViewPagerAdapter1(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new Test_F1();
        }
        else if (position == 1)
        {
            fragment = new Test_F2();
        }
        else if (position == 2)
        {
            fragment = new Test_F3();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Tab-1";
        }
        else if (position == 1)
        {
            title = "Tab-2";
        }
        else if (position == 2)
        {
            title = "Tab-3";
        }
        return title;
    }
}
