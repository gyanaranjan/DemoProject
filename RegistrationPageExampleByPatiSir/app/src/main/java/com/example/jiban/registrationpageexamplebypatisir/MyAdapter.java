package com.example.jiban.registrationpageexamplebypatisir;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Jiban on 26-Feb-18.
 */

public class MyAdapter extends BaseAdapter {
    CommonUtilities commonUtilities;
    ArrayList<ListingModel> lisitingArray;
    Context context;

    public MyAdapter(ArrayList<ListingModel> list, Context context, String value) {
        commonUtilities = new CommonUtilities();
        this.lisitingArray = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lisitingArray.size();
    }

    @Override
    public Object getItem(int i) {
        return lisitingArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lisitingArray.size();
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.show_data, parent, false);
        }
        final ListingModel model = lisitingArray.get(position);
        TextView firstname = (TextView) convertView.findViewById(R.id.editText);
        TextView email = (TextView) convertView.findViewById(R.id.editText2);
        TextView age = (TextView) convertView.findViewById(R.id.textView11);
        TextView gender = (TextView) convertView.findViewById(R.id.editText3);
        TextView village1= (TextView) convertView.findViewById(R.id.textView2);
        TextView post1= (TextView) convertView.findViewById(R.id.textView4);
        TextView district1= (TextView) convertView.findViewById(R.id.textView3);
        TextView village2= (TextView) convertView.findViewById(R.id.textView6);
        TextView post2= (TextView) convertView.findViewById(R.id.textView7);
        TextView district2= (TextView) convertView.findViewById(R.id.textView8);
        TextView volunter = (TextView)convertView.findViewById(R.id.textView10);
        ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);
//        TextView facebook= (TextView) convertView.findViewById(R.id.facebookID);
//        TextView twitter= (TextView) convertView.findViewById(R.id.twitterID);
//        TextView socialmedia= (TextView) convertView.findViewById(R.id.socialmedia);
//        TextView grapgicsdesign= (TextView) convertView.findViewById(R.id.graphicsdesign);
//        TextView citizensurvey= (TextView) convertView.findViewById(R.id.citizensurvey);
//        TextView tellecalling= (TextView) convertView.findViewById(R.id.tellecalling);




        final ImageView image = (ImageView) convertView.findViewById(R.id.image);

        firstname.setText(model.getFirstname());

        email.setText(model.getEmail());
        age.setText(model.getAge());
        gender.setText(model.getEmail());
        village1.setText(model.getVillage1());
        post1.setText(model.getPost1());
        district1.setText(model.getDistrict1());
        village2.setText(model.getVillage2());
        post2.setText(model.getPost2());
        district2.setText(model.getDistrict2());



        ImageLoader imageLoader = AppSingleton.getInstance(context).getImageLoader();
        imageLoader.get(model.getImgurl(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("IMAGE LOADING ERROR", "Image Load Error: " + error.getMessage());

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                Bitmap image_bitmap = response.getBitmap();
                if (image_bitmap != null) {
                    image.setImageBitmap(image_bitmap);
                }

            }
        });
        // image.setImageBitmap(model.getImage());
        return convertView;
    }
}
