package com.example.gyana.alltestingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Test_getter_setter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_getter_setter);
        Modal modal = new Modal();
        modal.setName("Gyana");
        Intent intent = new Intent(this,get_setter_getter_value.class);
       intent.putExtra("modal",""+(Object) modal);
        startActivity(intent);
    }
}
