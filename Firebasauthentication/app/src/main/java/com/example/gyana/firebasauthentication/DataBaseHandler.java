package com.example.gyana.firebasauthentication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gyana on 7/3/18.
 */

public class DataBaseHandler extends SQLiteOpenHelper {
    public static String DATABASE_NAME = "visitor.sqlite";
    public static final int DATABASE_VERSION = 1;
    public static final String FILTER_DATA = "Data_table";
    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // 3rd argument to be passed is CursorFactory instance
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + FILTER_DATA + "(NAME TEXT,PHONE TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }
}
