package com.example.gyana.alltestingapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthentication extends AppCompatActivity {
private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener fa;
    private FirebaseUser firebaseUser;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_authentication);
  button = (Button)findViewById(R.id.button);
        firebaseUser = firebaseAuth.getCurrentUser();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    firebaseAuth = FirebaseAuth.getInstance();
        fa = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("OBJECT", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("OBJECT", "onAuthStateChanged:signed_out");
                }
            }
        };
        firebaseAuth.createUserWithEmailAndPassword("gyanaranjan.mohapatra@afixi.com", "9583500758")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(FirebaseAuthentication.this, "failed",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
        firebaseAuth.signInWithEmailAndPassword("gyanaranjan.mohapatra@afixi.com", "9583500758")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(FirebaseAuthentication.this, "failed",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
    }
    @Override
    public void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(fa);
    }
    @Override
    public void onStop(){
        super.onStop();
        if (fa != null) {
            firebaseAuth.removeAuthStateListener(fa);
        }
    }

}
