package com.example.jiban.registrationpageexamplebypatisir;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ResultActivity extends AppCompatActivity {
    ListView listview;

    ProgressDialog progressDialog;
    CommonUtilities commonUtilities;
    ArrayList<ListingModel> arrayList = new ArrayList<>();
    MyAdapter arrayAdapter;
    ListingModel listingModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        commonUtilities = new CommonUtilities();
        listview=(ListView)findViewById(R.id.listView);

        try {
            getListing();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getListing() throws JSONException {
        progressDialog = ProgressDialog.show(ResultActivity.this, "", "please wait");

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("request_type","testlisting");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, CommonUtilities.LIVE,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("TEST", String.valueOf(response));
                        progressDialog.dismiss();
                        try {
                            JSONArray jsonArray = response.getJSONArray("data");

                            for(int count = 0;count<jsonArray.length();count++){
                                listingModel = new ListingModel();

                                JSONObject jsonObject2 = jsonArray.getJSONObject(count);

                                String id_user = jsonObject2.getString("id_user");
                                String firstname = jsonObject2.getString("firstname");
                                String lastname = jsonObject2.getString("lastname");
                                String email = jsonObject2.getString("email");
                                int phoneno1 = jsonObject2.getInt("phoneno1");
                                int phoneno2 = jsonObject2.getInt("phoneno2");
                                int age = jsonObject2.getInt("age");
                                int gender = jsonObject2.getInt("gender");
                                String village1 = jsonObject2.getString("village1");
                                String post1 = jsonObject2.getString("post1");
                                String distict1 = jsonObject2.getString("district1");
                                String village2 = jsonObject2.getString("village2");
                                String post2 = jsonObject2.getString("post2");
                                String distict2 = jsonObject2.getString("district2");
                                String  facebook = jsonObject2.getString("facebookid");
                                String twitter = jsonObject2.getString("twitterid");
                                String image=jsonObject2.getString("profile_image");
                                listingModel.setFirstname(firstname);
                                listingModel.setLastname(lastname);
                                listingModel.setEmail(email);
                                listingModel.setPhone1(phoneno1);
                                listingModel.setPhone2(phoneno2);
                                listingModel.setGender(gender);
                                listingModel.setAge(age);
                                listingModel.setVillage1(village1);
                                listingModel.setPost1(post1);
                                listingModel.setDistrict1(distict1);
                                listingModel.setVillage2(village2);
                                listingModel.setPost2(post2);
                                listingModel.setDistrict2(distict2);
                                listingModel.setFirstname(facebook);
                                listingModel.setTwitter(twitter);

                                String imgurl="http://192.168.1.58/visitorsworld/image/profile_image/"+id_user+"_"+image;
                                listingModel.setImgurl(imgurl);
                                arrayList.add(listingModel);
                                arrayAdapter =new MyAdapter(arrayList,ResultActivity.this,"listing");
                                listview.setAdapter(arrayAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        commonUtilities.showVolleyErrorMessage(getApplicationContext(), error, getResources().getString(R.string.network_unavailable));
                    }
                }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                commonUtilities.showNewtworkResponse(response);
                return super.parseNetworkResponse(response);
            }

        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(50),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "TEST");

    }
}
