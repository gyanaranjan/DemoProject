package com.example.gyana.alltestingapp;

/**
 * Created by gyana on 24/10/17.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gyana on 1/5/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    public static Cursor cursor;
    public static SQLiteDatabase sqLiteDatabase;
    public DatabaseHandler(Context context) {
        super(context, CommonUtilities.DATABASE_NAME, null, CommonUtilities.DATABASE_VERSION);
        // 3rd argument to be passed is CursorFactory instance
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + CommonUtilities.EMPLOYEE_TABLE_NAME + "(Number TEXT,Name TEXT,Email TEXT,Password TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);

    }
}