package com.example.gyana.alltestingapp;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class FirstFragment extends Fragment {
EditText name,number;
    Button button;
    View view;
    public SQLiteDatabase DB;
    CommonUtilities commonUtilities;
    TabLayout tabLayout;
    ViewPager viewPager;
    public FirstFragment() {
// Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final DatabaseHandler db = new DatabaseHandler(getActivity());
        db.getWritableDatabase();
        DB = getActivity().openOrCreateDatabase(CommonUtilities.DATABASE_NAME, MODE_PRIVATE, null);
        commonUtilities = new CommonUtilities();
        Log.v("SIPUN","First_U");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_first_fragment,container,false);
        tabLayout = (TabLayout)view.findViewById(R.id.textView);
        viewPager = (ViewPager)view.findViewById(R.id.viewpage);

        ViewPagerAdapter1  viewPagerAdapter = new ViewPagerAdapter1(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        //tableLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
       /* name = (EditText)view.findViewById(R.id.name);
        number = (EditText)view.findViewById(R.id.number);
        button = (Button)view.findViewById(R.id.button);*/
        Log.v("SIPUN","First_L");
       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("Number",number.getText().toString());
                contentValues.put("Name",name.getText().toString());
                DB.insert(commonUtilities.EMPLOYEE_TABLE_NAME, null, contentValues);
            }
        });*/
        return view;
    }

}
