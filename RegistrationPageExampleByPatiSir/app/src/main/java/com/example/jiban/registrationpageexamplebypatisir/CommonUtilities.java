package com.example.jiban.registrationpageexamplebypatisir;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

/**
 * Created by Jiban on 25-Feb-18.
 */

public class CommonUtilities {
    public static String LIVE = "http://bjd.org.in/public/user/add" ;
    public static String LOCAL = "http://192.168.1.40/e-odisha/public/user/add" ;

    private static String live_image = "";

    public static void showAlert(final Context context, String msg) {
        if (msg == null || msg.equalsIgnoreCase("null") || msg.equals("")) {
            // msg = context.getResources().getString(R.string.short_delay_msg);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg);

        final String finalMsg = msg;
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
//        builder.show();
        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
//            nbutton.setTextColor(context.getResources().getColor(R.color.button_color));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
//            pbutton.setTextColor(context.getResources().getColor(R.color.button_color));

    }

    public static void changeLang(Context context, String lang) {
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    //This is a common function used for showing alert,toast as wll as snackbar
    public static void showMessageView(Context context, String msg_value, String message, View view) {
        if (msg_value.equals("0")) {
            showAlert(context, message);
        } else if (msg_value.equals("1")) {
            showToast(context, message);
        } else {
//                showSnackbar(view, message);
        }
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    //for creating Md5


    public void showVolleyErrorMessage(Context context, VolleyError error, String Message) {
//    Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
        // Copied following error handling code from the net
        // Todo Replace this with a function in the AppSingleton such that

        // it can be used across all functions on the app
        if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
            Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();

//            Toast.makeText(context,context.getResources().getString(R.string.communication_error)  + Message, Toast.LENGTH_SHORT).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context,context.getResources().getString(R.string.auth_error)+ Message, Toast.LENGTH_SHORT).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context,context.getResources().getString(R.string.server_error )+ Message, Toast.LENGTH_SHORT).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context,context.getResources().getString(R.string.network_error) + Message, Toast.LENGTH_SHORT).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//            Toast.makeText(context,context.getResources().getString(R.string.parse_error) + Message, Toast.LENGTH_SHORT).show();
        }
    }

    public void getAndSetImage(String profileImageUrl, final ImageView imageView, Context context) throws JSONException {
        ImageLoader imageLoader = AppSingleton.getInstance(context).getImageLoader();
        imageLoader.get(profileImageUrl, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("IMAGE LOADING ERROR", "Image Load Error: " + error.getMessage());

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                Bitmap image_bitmap = response.getBitmap();
                if (image_bitmap != null) {
                    imageView.setImageBitmap(image_bitmap);
                }

            }
        });

    }
    public void showNewtworkResponse(NetworkResponse response) {
        int mStatusCode = response.statusCode;
        System.out.println("Response Status Code : " + mStatusCode);

        String responseString = null;
        try {
            responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("Response Result ===>" + responseString);

    }


    public boolean hasPermissions(Context context, int permissionCode, String... permissions) {
        Boolean allPermissionsGranted = true;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    allPermissionsGranted = false;
                }
            }
        }
        if (allPermissionsGranted) {
            allPermissionsGranted = true;
        } else {
            ActivityCompat.requestPermissions((Activity) context, permissions, permissionCode);
            allPermissionsGranted = false;
        }
        return allPermissionsGranted;
    }
}
