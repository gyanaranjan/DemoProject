package com.example.gyana.imagehandling;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class Imagehandling extends AppCompatActivity {
    Button send;
    ImageView camera,gallery,Imageview,showimage;
    Bitmap imagebitmap;
    public int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    String Profile_image="";
    String url="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagehandling);
        url = "http://sridhar.afixiindia.com/flexytiny_new/generaltask/imagestore";
        camera = (ImageView)findViewById(R.id.camera);
        gallery=(ImageView)findViewById(R.id.gallery);
        send = (Button)findViewById(R.id.button_send);
        Imageview = (ImageView)findViewById(R.id.imageview);
        showimage = (ImageView)findViewById(R.id.showimage);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(Imagehandling.this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(Imagehandling.this,new String[]{Manifest.permission.CAMERA},PERMISSION_ACCESS_COARSE_LOCATION );
                }
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


                startActivityForResult(takePictureIntent, 3);
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "SELECT FILE"), 2);
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    imagebitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageBytes = baos.toByteArray();
                    Profile_image = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                }
                HashMap hashMap = new HashMap();
                hashMap.put("Image",Profile_image);
                JSONObject jsonObject = new JSONObject(hashMap);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("msg").contains("sucess")){
                                String image = response.getString("image");
                                String url_image = "http://sridhar.afixiindia.com/flexytiny_new/image/"+image;
                                getAndSetImage(url_image, showimage, Imagehandling.this);

                                Toast.makeText(Imagehandling.this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Imagehandling.this, ""+error, Toast.LENGTH_SHORT).show();
                    }
                });
                AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"TEST");

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == 2) {
            //Imageview = null;

            if (data != null) {
                try {
                    imagebitmap = MediaStore.Images.Media.getBitmap(Imagehandling.this.getContentResolver(), data.getData());
                    imagebitmap = Bitmap.createScaledBitmap(imagebitmap, 100, 100, true);
                    Imageview.setImageBitmap(imagebitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        if (requestCode==3){
            if (data!=null){
                imagebitmap=(Bitmap)data.getExtras().get("data");
                Imageview.setImageBitmap(imagebitmap);
            }
        }
    }
    public void getAndSetImage(String profileImageUrl, final ImageView imageView, Context context) throws JSONException {
        ImageLoader imageLoader = AppSingleton.getInstance(context).getImageLoader();
        imageLoader.get(profileImageUrl, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("STRING_TAG", "Image Load Error: " + error.getMessage());

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                Bitmap getImage = response.getBitmap();
                if (getImage != null) {
                    showimage.setImageBitmap(getImage);
                }

            }
        });

    }

}
