package com.example.gyana.alltestingapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by gyana on 1/5/17.
 */

class CommonUtilities {
    public static final String EMPLOYEE_TABLE_NAME = "employee_table_name";
    public static final String DATEANDTIME = "dateandtime";
    public static final String KHETNAME = "khetname";
    public static final String CHAKNAME = "chakname";
    public static final String CONNECTIONDETECTOR = "connectiondetector";
    public static final String CONNECTIONDETECTOR2 = "connectiondetector2";
    public static final String CHECKINTEGER = "checkinteger";
    public SQLiteDatabase DB;

    public static String DATABASE_NAME = "visitor.sqlite";
    public static final int DATABASE_VERSION = 1;
    public ArrayList getdata(ArrayList Data,ArrayList Count){
        Cursor c1 = DB.rawQuery(" SELECT * FROM " + CommonUtilities.DATEANDTIME, null);
        if (c1.getCount() > 0) {
            if (c1.moveToFirst()) {

                do {
                    String data = c1.getString(0);
                    String count = c1.getString(1);
                    Data.add(data);
                    Data.add(count);

//                            if (s.equals(employee_number)){
//                                Toast.makeText(DataBaseExample.this, "succesfull", Toast.LENGTH_SHORT).show();
//                            }else{
//                                Toast.makeText(DataBaseExample.this, "data not found", Toast.LENGTH_SHORT).show();
//                            }
                } while (c1.moveToNext());
            }
        }
        return Data;
    }
}
