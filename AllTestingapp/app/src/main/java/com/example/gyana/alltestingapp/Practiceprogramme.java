package com.example.gyana.alltestingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.HashMap;
import java.util.IdentityHashMap;

public class Practiceprogramme extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practiceprogramme);
        HashMap hashMap = new HashMap();
        hashMap.put(new String("1"),"A");
        hashMap.put("1","B");

        IdentityHashMap identityHashMap = new IdentityHashMap();
        identityHashMap.put(new String("1"),"A");
        identityHashMap.put("1","B");
        Toast.makeText(this, "succes", Toast.LENGTH_SHORT).show();
        String A = "ram";
        String B = new String("ram");
        if (A.equals(B)){
            Toast.makeText(this, "success equals", Toast.LENGTH_SHORT).show();
        }
        if (A==B){
            Toast.makeText(this, "Success ==", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "failed==", Toast.LENGTH_SHORT).show();
        }
    }
}
