package com.example.gyana.alltestingapp.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gyana.alltestingapp.R;

import java.util.ArrayList;

public class WordFragment extends Fragment {
    View view;
    ListView listView;
    ArrayList<String> arrayList = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_word, container, false);
        listView = (ListView)view.findViewById(R.id.wordfrag);
        arrayList.add("Apple");
        arrayList.add("Ant");
        arrayList.add("Axe");
        arrayList.add("Aeroplane");
        WordFragmentadapter wordFragmentadapter = new WordFragmentadapter(getActivity(),arrayList);
        listView.setAdapter(wordFragmentadapter);
        return view;
    }
    public void display(String value){
        if (value.equals("0")){
           arrayList.clear();
            arrayList.add("Apple");
            arrayList.add("Ant");
            arrayList.add("Axe");
            arrayList.add("Aeroplane");
        }
        if (value.equals("1")){
            arrayList.clear();
            arrayList.add("Bubble");
            arrayList.add("Box");
            arrayList.add("Ballon");
            arrayList.add("Bars");
        }
        if (value.equals("2")){
            arrayList.clear();
            arrayList.add("Cat");
            arrayList.add("Coffee");
            arrayList.add("Cannel");
            arrayList.add("Chair");
        }
        if (value.equals("3")){
            arrayList.clear();
            arrayList.add("Dipu");
            arrayList.add("Debendra");
            arrayList.add("Duck");
            arrayList.add("Dairy");
        }
        WordFragmentadapter wordFragmentadapter = new WordFragmentadapter(getActivity(),arrayList);
        listView.setAdapter(wordFragmentadapter);
    }

}

class WordFragmentadapter extends BaseAdapter {
    Context context;
    ArrayList arrayList;
    public WordFragmentadapter(Context context,ArrayList arrayList){
        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.itemtype, parent, false);
        }
        TextView textView = (TextView)convertView.findViewById(R.id.textview);
        EditText editText = (EditText)convertView.findViewById(R.id.edittext);
        textView.setText((String)arrayList.get(position));

        return convertView;
    }
}
