package com.example.gyana.alltestingapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class ThirdFragment extends Fragment {
    TextView textView;
    View view;
    public SQLiteDatabase DB;
    CommonUtilities commonUtilities;
    public ThirdFragment() {
// Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Toast.makeText(getActivity(), "thirds", Toast.LENGTH_SHORT).show();

        final DatabaseHandler db = new DatabaseHandler(getActivity());
        db.getWritableDatabase();
        DB = getActivity().openOrCreateDatabase(CommonUtilities.DATABASE_NAME, MODE_PRIVATE, null);
        commonUtilities = new CommonUtilities();
        Log.v("SIPUN","third_u");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_third_fragment, container, false);
        textView = (TextView)view.findViewById(R.id.third);
        Log.v("SIPUN","third_b");

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                Cursor cursor = DB.rawQuery(" SELECT * FROM " + CommonUtilities.EMPLOYEE_TABLE_NAME ,null);
                if (cursor.getCount()>0){
                    cursor.moveToFirst();
                    Toast.makeText(getActivity(), "name is = "+ cursor.getString(1) + "\n" + " number is ="+cursor.getString(0) + "\n" +"Email is = "+ cursor.getString(2) + "\n"+"password is = "+cursor.getString(3), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

}
