package com.example.gyana.alltestingapp;

/**
 * Created by gyana on 26/10/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();

}
