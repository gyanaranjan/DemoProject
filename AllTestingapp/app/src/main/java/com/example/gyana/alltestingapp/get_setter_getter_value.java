package com.example.gyana.alltestingapp;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class get_setter_getter_value extends AppCompatActivity {
ListView listView;
Button button4;
    Spinner[] spinner,spinner2;
    ArrayList color = new ArrayList();
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_setter_getter_value);
       /* String value = getIntent().getStringExtra("modal");
        Object object = value;*/
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setLayoutParams(new ActionBar.LayoutParams(Gravity.RIGHT));
        listView = (ListView)findViewById(R.id.listview);
        button4 = (Button)findViewById(R.id.button4);
        ArrayList arrayList = new ArrayList();
        arrayList.add("Red");
        arrayList.add("White");
        arrayList.add("Black");
        ArrayList size_list = new ArrayList();
        size_list.add("Large");
        size_list.add("Small");
        size_list.add("Medium");
        final get_setter_getter_valueAdapter get_setter_getter_valueAdapter = new get_setter_getter_valueAdapter(this,arrayList,size_list);
        listView.setAdapter(get_setter_getter_valueAdapter);
       /* button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0;i<spinner.length;i++){
                   String data =  spinner[i].getSelectedItem().toString();
                    Toast.makeText(get_setter_getter_value.this, data, Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(get_setter_getter_value.this,Test_getter_setter.class);
                startActivity(intent);
            }
        });
    }
    class get_setter_getter_valueAdapter extends BaseAdapter{

        Context context;
        ArrayList arrayList;
        ArrayList sizelist;
        public get_setter_getter_valueAdapter(Context context,ArrayList arrayList,ArrayList sizelist){
            this.context = context;
            this.arrayList = arrayList;
            this.sizelist = sizelist;
        }
        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                convertView = inflater.inflate(R.layout.spinner_layout, parent, false);
            }
            Spinner spinner = (Spinner)convertView.findViewById(R.id.spinner);
           Spinner spinner2 = (Spinner)convertView.findViewById(R.id.spinner2);

            ArrayAdapter arrayAdapter = new ArrayAdapter(context,R.layout.support_simple_spinner_dropdown_item,arrayList);
            spinner.setAdapter(arrayAdapter);

            ArrayAdapter arrayAdapter12 = new ArrayAdapter(context,R.layout.support_simple_spinner_dropdown_item,sizelist);
            spinner2.setAdapter(arrayAdapter12);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   String data = parent.getSelectedItem().toString();
                    Toast.makeText(get_setter_getter_value.this, ""+data, Toast.LENGTH_SHORT).show();
                    Toast.makeText(get_setter_getter_value.this, ""+position, Toast.LENGTH_SHORT).show();
                    if (!color.contains(data)){
                        color.add(data);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(get_setter_getter_value.this, ""+position, Toast.LENGTH_SHORT).show();
                    String data = parent.getSelectedItem().toString();
                    Toast.makeText(get_setter_getter_value.this, ""+data, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            return convertView;
        }
    }

}
