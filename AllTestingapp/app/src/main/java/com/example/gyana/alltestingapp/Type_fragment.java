package com.example.gyana.alltestingapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Type_fragment extends Fragment {
    ListView list;
    View view;
    TextView textView;
    ArrayList<String> arrayList = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v("SIPUN","First_U");
        arrayList.add("Cricket player");
        arrayList.add("Football player");
        arrayList.add("chess");
        arrayList.add("Country");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_type_fragment, container, false);
        list = (ListView)view.findViewById(R.id.listview);
        FirstFragment ldf1 = new FirstFragment ();
        Bundle args = new Bundle();
        args.putString("YourKey", "YourValue");
        ldf1.setArguments(args);
        FragmentManager fm = getFragmentManager();
       /* textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubType_frament fragment = new SubType_frament();
                final Bundle bundle = new Bundle();
                bundle.putString("id_User", "Sipun");
                Log.i("BUNDLE", bundle.toString());
                fragment.setArguments(bundle);
                FragmentManager fm = getFragmentManager();
                android.app.FragmentTransaction transaction = fm.beginTransaction();
                transaction.replace(R.id.fragment2, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

//Inflate the fragment
            }
        });*/

        CustomlistAdapter custom = new CustomlistAdapter(getActivity(),arrayList,fm);
        list.setAdapter(custom);
// get the reference of Button
// perform setOnClickListener on second Button

        return view;
    }
}
class CustomlistAdapter extends BaseAdapter{
    Context context;
    ArrayList<String> arrayList;
    FragmentManager fm1;
    public CustomlistAdapter(Context context,ArrayList<String> arrayList,FragmentManager fm){
        this.context = context;
        this.arrayList = arrayList;
        this.fm1 = fm;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.itemtype, parent, false);
        }
        TextView text = (TextView)convertView.findViewById(R.id.textview);
        text.setText(arrayList.get(position));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubType_frament fragment = new SubType_frament();
                final Bundle bundle = new Bundle();
                bundle.putString("id_User", arrayList.get(position));
                Log.i("BUNDLE", bundle.toString());
                fragment.setArguments(bundle);
                android.app.FragmentTransaction transaction = fm1.beginTransaction();
                transaction.replace(R.id.fragment2, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return convertView;
    }
}