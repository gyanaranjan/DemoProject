package com.example.gyana.firebasauthentication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class DesignActivity extends AppCompatActivity {
    Button button3;
    SQLiteDatabase sqLiteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design);
        DataBaseHandler dataBaseHandler = new DataBaseHandler(DesignActivity.this);
        dataBaseHandler.getWritableDatabase();
        sqLiteDatabase = DesignActivity.this.openOrCreateDatabase(DataBaseHandler.DATABASE_NAME, MODE_PRIVATE, null);

        button3 = (Button)findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               insert_data();
            }
        });
    }
    public void insert_data(){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME","sipun");
        contentValues.put("PHONE","9583500758");
        sqLiteDatabase.insert(DataBaseHandler.FILTER_DATA, null, contentValues);


        Cursor cursor = sqLiteDatabase.rawQuery(" SELECT * FROM " + DataBaseHandler.FILTER_DATA, null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                Toast.makeText(this, "name = "+cursor.getString(0), Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "name = "+cursor.getString(1), Toast.LENGTH_SHORT).show();
            }while (cursor.moveToNext());

        }
    }
}
