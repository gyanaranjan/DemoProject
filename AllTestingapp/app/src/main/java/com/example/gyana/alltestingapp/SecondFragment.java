package com.example.gyana.alltestingapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class SecondFragment extends Fragment {
    public SQLiteDatabase DB;
    CommonUtilities commonUtilities;
    EditText name,number;
    Button button;
    View view;
    public SecondFragment() {
// Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DatabaseHandler db = new DatabaseHandler(getActivity());
        db.getWritableDatabase();
        DB = getActivity().openOrCreateDatabase(CommonUtilities.DATABASE_NAME, MODE_PRIVATE, null);
        commonUtilities = new CommonUtilities();
        Log.v("SIPUN","second_U");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_second_fragment,container,false);
        name = (EditText)view.findViewById(R.id.name);
        number = (EditText)view.findViewById(R.id.number);
        button = (Button)view.findViewById(R.id.button);
        Log.v("SIPUN","second_b");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), name.getText().toString()+","+number.getText().toString(), Toast.LENGTH_SHORT).show();
                DB.execSQL(" UPDATE " + CommonUtilities.EMPLOYEE_TABLE_NAME + " SET Email = '" + name.getText().toString() + "',Password = '" + number.getText().toString() + "' WHERE Number = '" + "9583500758" + "'");
            }
        });
    return view;
    }

}
